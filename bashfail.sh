#!/bin/bash
function error
{
	name=`basename ${BASH_COMMAND%% *}`
	~/bin/failface.sh $name
}
trap error ERR
