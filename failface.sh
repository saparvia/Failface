#!/bin/bash
HAARFILE=$HOME/haarcascade_frontalface_alt.xml
name=$1
if [[ $name == "" ]]; then
	name="unknown"
fi
dir=$HOME/fails
mkdir -p $dir
outfile=$dir/${name}_`date +%Y%m%d-%H%M%S`.png
python ~/bin/grabface.py --haarfile $HAARFILE >$outfile
gwenview $outfile &
