#!/usr/bin/env python
import optparse
import time

import cv

def main():
	parser = optparse.OptionParser()
	parser.add_option('--camera', dest='camera_num', help='Camera to use, starts from 0', type='int', default=0)
	parser.add_option('--margins', dest='margins', help='Comma separated list of margins to add to the detected face box: left,top,right,bottom', type='string', default='0,0,0,0')
	parser.add_option('--delay', dest='delay', help='Delay before grabbing frame (does not include time to start this program, start the camera, etc.) in seconds', type='float', default=0.0)
	parser.add_option('--haarfile', dest='haarfile', help='Path to haar cascade file needed for face detection', type='string', default='haarcascade_frontalface_alt.xml')

	(options, _) = parser.parse_args()

	time.sleep(options.delay)

	cap = cv.CaptureFromCAM(options.camera_num)
	img = cv.QueryFrame(cap)

	# Try to do as little as possible before grabbing camera
	import sys
	import tempfile
	import os

	from PIL import Image

	if options.margins != '':
		margins = [int(x) for x in options.margins.split(',')]

	image_size = cv.GetSize(img)

	# I couldn't figure out how to just save a region, so save the whole thing, and crop later
	temp = tempfile.mkstemp(suffix='.png')[1]
	cv.SaveImage(temp, img)

	face_region = None
	if os.path.exists(options.haarfile):
		# Detect face
		cascade = cv.Load(options.haarfile)
		faces = cv.HaarDetectObjects(img, cascade, cv.CreateMemStorage(), min_size=(50, 50)) # This Size might need tuning
		for (x, y, width, height), _ in faces:
			x1 = max(x-margins[0] ,0)
			y1 = max(y-margins[1] ,0)
			x2 = min(x+width+margins[2] ,image_size[0])
			y2 = min(y+height+margins[3] ,image_size[1])
			face_region = ((x1, y1, x2, y2))
			break 

	pil_image = Image.open(temp)
	if face_region:
		pil_image = pil_image.crop((x1, y1, x2, y2))

	pil_image.save(sys.stdout, format='png')
	os.remove(temp)

if __name__ == '__main__':
	main()
